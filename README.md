## README - Express.js Backend for Task Management Application

This repository contains the backend codebase for a Task Management Application built using Express.js.

### Getting Started

To get started with the backend server, follow these steps:

1. **Clone the Repository:**
   ```bash
   git clone <repository_url>
   ```

2. **Install Dependencies:**
   ```bash
   cd express-backend-task-management
   npm install
   ```

3. **Set Up Environment Variables:**
   Create a `.env` file in the root directory and add the following environment variables:
   ```
   PORT=3000
   ```

4. **Run the Server:**
   ```bash
   npm start
   ```
   This will start the server on port 3000 by default. You can access the endpoints via `http://localhost:3000`.

### API Endpoints

The backend server provides the following API endpoints:

#### User Endpoints

- **GET /api/user/:id:** Get user details by ID.
- **POST /api/user:** Create a new user.
- **PUT /api/user/:id:** Update user details by ID.
- **DELETE /api/user/:id:** Delete user by ID.

#### Task List Endpoints

- **GET /api/lists:** Get all task lists.
- **GET /api/lists/:id:** Get task list details by ID.
- **POST /api/lists:** Create a new task list.
- **PUT /api/lists/:id:** Update task list details by ID.
- **DELETE /api/lists/:id:** Delete task list by ID.

#### Task Endpoints

- **GET /api/lists/:listId/tasks:** Get all tasks in a specific list.
- **GET /api/lists/:listId/tasks/:taskId:** Get task details by ID in a specific list.
- **POST /api/lists/:listId/tasks:** Create a new task in a specific list.
- **PUT /api/lists/:listId/tasks/:taskId:** Update task details by ID in a specific list.
- **DELETE /api/lists/:listId/tasks/:taskId:** Delete task by ID in a specific list.

### Data Models

The backend server operates with the following data models:

#### User Model
```json
{
  "name": "Vicky",
  "email": "vicky@example.com",
  "settings": {
    "theme": "light",
    "notifications": {
      "email": true,
      "push": true
    }
  }
}
```

#### Task List Model
```json
{
  "id": 1,
  "name": "Personal",
  "tasks": [
    {
      "id": 1,
      "title": "Complete your first task",
      "description": "This is your first task in the app.",
      "status": "incomplete",
      "due_date": "2024-04-07",
      "reminders": [
        {
          "time": "2024-04-07T10:00:00",
          "method": "push"
        }
      ]
    },
    {
      "id": 2,
      "title": "Create new list",
      "description": "Create a new list to organize your tasks.",
      "status": "incomplete",
      "due_date": null
    }
  ]
}
```

### Contributing

Contributions are welcome! If you have any suggestions, improvements, or feature requests, feel free to open an issue or create a pull request.

### License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.